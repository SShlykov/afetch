defmodule Afetch.MixProject do
  use Mix.Project
  @source_url "git@172.25.78.108:scandinavians/afetch.git"
  @version "0.0.1"

  def project do
    [
      app: :afetch,
      version: @version,
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      package: package(),
      description: description(),
      deps: deps(),
      name: "Afetch",
      docs: docs(),
    ]
  end

  defp description do
    """
    Library for doing whatever you want.
    """
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE* CHANGELOG*),
      maintainers: ["NIAC DEVELOPMENT TEAM"],
      licenses: ["MIT"],
      links: %{}
    ]
  end

  defp docs do
    [
      main: "Afetch",
      source_ref: "v#{@version}",
      source_url: @source_url,
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Afetch.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:poison, "~> 4.0"},
    ]
  end
end
