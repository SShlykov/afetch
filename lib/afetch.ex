defmodule Afetch do
  @moduledoc """
  Documentation for `Afetch`.
  """

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_, opts), do: start_link(opts)
  @doc false
  @spec start_link(any) :: {:error, any} | {:ok, pid}
  def start_link(opts), do: Afetch.Application.start(:normal, opts)

  @doc """
  Hello world.

  ## Examples

      iex> Afetch.split_messages(message, max_byte_size)
      [{msg, idx}, ...]

  """
  def split_messages(message, max_byte_size) do
    size = byte_size(message)
    n = ceil(Float.floor((size/max_byte_size), 0) + 1) |> IO.inspect


    (1..(n + 1))
    |> Enum.reduce(
      {message, []},
      fn _part, {c_msg, list} ->
        {splitted, tail_spl} = String.split_at(c_msg, max_byte_size)
        new_list = [splitted | list]

        {tail_spl, new_list}
    end)
    |> elem(1)
    |> Enum.reverse()
    |> Enum.with_index(1)
  end
end
