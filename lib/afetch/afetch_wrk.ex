defmodule Afetch.Wrk do
  require Logger
  use GenServer

  def fetch(pid, options),            do: GenServer.call(pid, {:fetch, options})
  def state(pid),                     do: GenServer.call(pid, :state)
  def destroy(pid),                   do: GenServer.call(pid, :state)

  def set_initiator(pid, init),  do: GenServer.cast(pid, {:set_initiator, init})
  # Server (callbacks)

  def start_link(name), do: create(name)

  @impl true
  @spec init(any) :: {:ok, any}
  def init(state) do
    Process.flag(:trap_exit, true)
    {:ok, state}
  end

  def create({:via, Registry, {_, key}} = name) do
    GenServer.start_link(
      __MODULE__,
      %{comm_key: key, initiator: nil, messages: []},
      name: name
    )
  end

  def create(_any), do: {:error, :invalid_params}

  @doc """
  Afetch.DynamicFetcher.fetch(%{comm_key: "123", msg_id: 1, last_id: 4})
  """

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:fetch, %{last_id: lid, message: message, crc32_part: crc32_part} = options}, _from, %{messages: messages, initiator: initiator} = state) do
    new_state = Map.update(state, :messages, [options], fn msgs -> [options | msgs] end)
    cond do
      length(messages) + 1 == lid ->
        joined_message =
          new_state
          |> Map.get(:messages)
          |> Enum.sort_by(& &1.msg_id)
          |> Enum.map_join(& &1.message)

        if :erlang.crc32(joined_message) == options.crc32 do
          send(initiator, {:fetched, joined_message})

          {:reply, {:halt, new_state}, new_state}
        else
          Logger.error("Fetched and joined message with wrong crc32: #{joined_message} with crc32 #{:erlang.crc32(joined_message)} and message #{inspect(options)}")
          {:reply, {:halt, %{error: "message not full or not ordered correctly"}}, new_state}
        end

      (:erlang.crc32(message) !== crc32_part) ->
        Logger.error("Fetched message with wrong crc32: #{inspect(options)}")
        {:reply, {:cont, new_state}, new_state}

      true ->
        {:reply, {:cont, new_state}, new_state}
    end
  end

  @impl true
  def handle_cast({:set_initiator, {initiator, callback}}, state) do
    callback.(state)
    {:noreply, Map.put(state, :initiator, initiator)}
  end
end
