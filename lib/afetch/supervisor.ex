defmodule Afetch.DynamicFetcher do
  use DynamicSupervisor
  require Logger
  @supervisor_name :dynamic_fetcher
  @evt_base ~w(dynamic_fetcher boot)a
  alias Afetch.DynamicFetcher.Registry, as: ModuleRegistry
  alias Afetch.Wrk

  # Client

  def state(key) do
    case find(key) do
      {:ok, pid} ->
        Afetch.Wrk.state(pid)
      {:error, _} = e -> e
    end
  end

  def init_request(%{comm_key: key, initiator: initiator, callback: callback}) do
    case find(key) do
      {:ok, _pid} ->
        {:error, :exists}

      {:error, _} ->
        {:ok, pid} = start(key)
        Afetch.Wrk.set_initiator(pid, {initiator, callback})
        :ok
    end
  end

  def fetch(%{comm_key: key, msg_id: _mid} = options) do
    case find(key) do
      {:ok, pid}      ->
        case Afetch.Wrk.fetch(pid, options) do
          {:halt, _} = msg ->
            destroy(key)
            msg
          msg ->
            msg
        end
      {:error, _} = e -> e
    end
  end

  def destroy(key) do
    case find(key) do
      {:ok, pid} ->
        state = Afetch.Wrk.destroy(pid)
        DynamicSupervisor.terminate_child(@supervisor_name, pid) |> IO.inspect
        state

      {:error, _} = e -> e
    end
  end

  def list_engines do
    ModuleRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(& elem(&1, 0))
  end

  # Server funcs

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])
  end

  defp start(key) do
    name = {:via, Registry, {ModuleRegistry, key}}

    DynamicSupervisor.start_child(@supervisor_name, {Wrk, name})
  end

  # Private
  defp find(key) do
    case Registry.lookup(ModuleRegistry, key) do
      [] -> {:error, :unexisting_comm}
      [{pid, nil}] -> {:ok, pid}
    end
  end

  @spec events :: [[atom]]
  def events, do: Telemetry.Helpers.events(@evt_base, ~w(start stop exception)a)
end
