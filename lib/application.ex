defmodule Afetch.Application do
  @moduledoc false

  use Application
  @evt_base ~w(afetch boot)a

  @impl true
  def start(_type, _args) do
    children = attach([
      Afetch.DynamicFetcher,

    ]) ++ [
      {Registry, keys: :unique, name: Afetch.DynamicFetcher.Registry},
    ]

    opts = [strategy: :one_for_one, name: Afetch.Supervisor, max_restarts: 15]

    :telemetry.span(@evt_base, %{children: children, opts: opts}, fn -> {Supervisor.start_link(children, opts), %{}} end)
  end


  @spec events :: [[atom]]
  def events, do: Telemetry.Helpers.events(@evt_base, ~w(start stop exception)a)

  defp attach(list) do
    list = List.flatten(list)
    Telemetry.Helpers.attach_events([__MODULE__ | list])
    list
  end
end
