defmodule TelemetryHandler do
  @moduledoc """
  Telemetry handler
  """
  require Logger

  def handle_event(_evt, _measurement, %{children: _childs, opts: _opts}, _config) do
    # Logger.info("Agents boot begins. Children: #{inspect(childs)}. Opts: #{inspect(opts)}")
    :ok
  end

  def handle_event(~w[afetch boot stop]a,     pms, _metadata, _config), do: write_duration(pms, "Общая система")

  def handle_event(event, measurement, metadata, config) do
    params = %{measurement: measurement, metadata: metadata, config: config}

    Logger.warning("Неизвестное событие: [#{inspect(event)}]. Параметры: #{inspect(params)}")
  end

  def write_duration(%{duration: duration}, name), do: info("#{name} запущена за #{to_ms(duration)}мс")

  def to_ms(time), do: System.convert_time_unit(time, :native, :millisecond)

  defp info(text), do: (IO.ANSI.green() <> "[запуск] " <> IO.ANSI.reset() <> text) |> IO.puts()
end
